from django.db import models
import datetime

CATEGORY = [('Personal Time', 'Personal Time'), ('Friends', 'Friends'),
('Academics', 'Academics'), ('Family', 'Family'), ('Meeting', 'Meeting'),
('Others', 'Others')]

# Create your models here.
class Schedule(models.Model):
    id = models.AutoField(primary_key=True)
    activity = models.CharField(max_length=100)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length = 50)
    category = models.CharField(max_length = 50, choices = CATEGORY)
    description = models.TextField(max_length = 100)
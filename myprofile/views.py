from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'Home.html')

def index2(request):
    return render(request, 'Resume.html')

def index3(request):
    return render(request, 'About.html')
from django.urls import path
from .views import index, index2, index3

urlpatterns = [
    path('', index, name="homepage"),
    path('resume/', index2, name="resume"),
    path('about/', index3, name="about"),
]
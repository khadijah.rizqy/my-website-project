from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.shortcuts import redirect
import datetime

# Create your views here.
def index(request):
    return render(request, 'Home.html')

def index2(request):
    return render(request, 'Resume.html')

def index3(request):
    return render(request, 'About.html')

def sched_add(request):
    if request.method == 'POST' :
        form = ScheduleForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('story_5:add')
    else:
        form = ScheduleForm()

    content = {'title' : 'Schedule Form',
               'form' : form,
               'jadwal': Schedule.objects.all()
               }

    return render(request, 'schedule_inputform.html', content)

def sched_delete(request):
    if 'pk' in request.POST:
        pk = request.POST['pk']
        Schedule.objects.get(pk = pk).delete()
        content = {'title' : 'Schedule Form',
                'form' : ScheduleForm(request.POST),
                'jadwal': Schedule.objects.all()
                }

    return redirect('story_5:add')
from django.urls import path
from .views import sched_add, sched_delete

app_name = 'story_5'

urlpatterns = [
    path('schedule/', sched_add, name="add"),
    path('schedule/deleted/', sched_delete, name="delete"),
]
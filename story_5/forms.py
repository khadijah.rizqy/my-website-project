from django import forms
from django.db import models
from django.forms import ModelForm
from .models import Schedule

class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = ['activity', 'date', 'time', 'location', 'category', 'description']
        labels = {'date' : 'Date', 'time' : 'Time', 'activity' : 'Activity', 'location' : 'Location', 'category' : 'Category', 'description' : 'Description'}
        widgets = {
            'date' : forms.DateInput(attrs={'class' : 'form-control',
            'type' : 'date'}),
            'time' : forms.TimeInput(attrs={'class' : 'form-control',
            'type' : 'time'}),
            'activity' : forms.TextInput(attrs={'class' : 'form-control',
            'type' : 'text', 'placeholder': 'What will you do?'}),
            'location' : forms.TextInput(attrs={'class' : 'form-control',
            'type' : 'text', 'placeholder' : 'Where is it?'}),
            'category' : forms.Select(attrs={'class' : 'form-control'}),
            'description' : forms.TextInput(attrs={'class' : 'form-control',
            'type' : 'text', 'placeholder' : 'Is there any extra notes on the event?'})
        }